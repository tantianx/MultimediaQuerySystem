package audioProcessor;

import java.util.ArrayList;

//import org.opencv.core.Mat;






import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import tools.*;

public class AudioFFTFeature extends AudioFeature {
	
	public int channelNum;
	public static final int COEFFICIENT_NUM = 128;
	public static int IMAGE_COUNT_PER_TRANSFORM = 8;
	public static final int SUBSAMPLE_FOR_IMAGE = 100;
	public short subsampleData[][];
	public short subsampleData2[][];
	
	public double fftsin[][];
	public double fftcos[][];
	public double fftsin2[][];
	public double fftcos2[][];
	
	@Override
	public void getFeatureFromeAudioData(ArrayList<Byte> sampleArray1, ArrayList<Byte> sampleArray2, int channelNum, int frameSize, int imageNum)
	{
		double sample1[], sample2[];
		
		int sampleSize = frameSize / channelNum;
		int length = sampleArray1.size() / sampleSize;
		this.channelNum = channelNum;
		int len;
		
		if (length % 2 != 0)
			len = length + 1;
		else {
			len = length;
		}
		
		sample1 = new double[len];
		sample2 = new double[len];
		
		int temp;
		byte high1, high2, low1, low2;
		high2 = low2 = 0;
		temp = 0;
		for (int i = 0; i < length; i++) {
			high1 = sampleArray1.get(temp);
			if (channelNum == 2) high2 = sampleArray2.get(temp);
			temp++;
			low1 = sampleArray1.get(temp);
			if (channelNum == 2) low2 = sampleArray2.get(temp);
			temp++;
			sample1[i] = tools.DataTypeHelper.twoBytesToInt(high1, low1);
			if (channelNum == 2) sample2[i] = tools.DataTypeHelper.twoBytesToInt(high2, low2);
		}
		
		int i, j;
		double samplesPerImage = (double)len / (double)imageNum;
		int interval = (int) (samplesPerImage / SUBSAMPLE_FOR_IMAGE);
		
		this.subsampleData = new short[imageNum][SUBSAMPLE_FOR_IMAGE];
		this.subsampleData2 = new short[imageNum][SUBSAMPLE_FOR_IMAGE];
		
		this.fftsin = new double[imageNum][COEFFICIENT_NUM];
		this.fftcos = new double[imageNum][COEFFICIENT_NUM];
		this.fftsin2 = new double[imageNum][COEFFICIENT_NUM];
		this.fftcos2 = new double[imageNum][COEFFICIENT_NUM];
		
		int size = (int) (IMAGE_COUNT_PER_TRANSFORM * samplesPerImage);
		double N = Math.log(size) / Math.log(2);
		if (N - (int)N > 0) N++;
		int fftSize = (int) Math.pow(2, (int)N);
		
		int sub, count;
		int lowerBound, upperBound;
		Complex x[], x2[], y[], y2[];
		
		for (i = 0; i < imageNum; i++) {
			sub = (int) (i * samplesPerImage);
			for (j = 0; j < SUBSAMPLE_FOR_IMAGE; j++) {
				this.subsampleData[i][j] = (short) sample1[sub + j * interval];
				this.subsampleData2[i][j] = (short) sample1[sub + j * interval];
			}
			
			lowerBound = (int) ((i * samplesPerImage - fftSize / 2));
			upperBound = (int) ((i * samplesPerImage + fftSize / 2));
			
			x = new Complex[fftSize];
			x2 = new Complex[fftSize];
			
			y = new Complex[fftSize];
			y2 = new Complex[fftSize];
			
			count = 0;
			j = lowerBound;
			do {
				if (j >= 0 && j < len - 1) {
					x[count] = new Complex(sample1[j], 0);
					x2[count] = new Complex(sample2[j], 0);
				}
				else {
					x[count] = new Complex(0, 0);
					x2[count] = new Complex(0, 0);
				}
				count++;
				j++;
			} while(count < fftSize);
			//for (j = 0; j < fftSize; j++)
				//System.out.println("j:" + j + " count: " +count + " " + x[j].re());
			
			y = FFT.fft(x);
			y2 = FFT.fft(x2);
			
			for (j = 0; j < COEFFICIENT_NUM; j++) {
				this.fftsin[i][j] = y[j].re();
				this.fftcos[i][j] = y[j].im();
				
				this.fftsin2[i][j] = y2[j].re();
				this.fftcos2[i][j] = y2[j].im();
			}
		}
		
		System.out.println("FFT feature recored!");
	}

	@Override
	public double featureDiff(AudioFeature af, int imgNum1, int imgNum2, int frameNum) {
		AudioFFTFeature af2 = (AudioFFTFeature) af;
		int i, j;
		
		double diff = 0;
		double similarity = 0;
		double sample1, sample2;
		
		
		for (j = 0; j < frameNum; j++) {
			/*
			for (i = 0; i < SUBSAMPLE_FOR_IMAGE; i++) {
				sample1 = this.subsampleData[imgNum1 + j][i];
				sample2 = af2.subsampleData[imgNum2 + j][i];
				diff += Math.abs(sample1 - sample2);
				
				sample1 = this.subsampleData2[imgNum1 + j][i];
				sample2 = af2.subsampleData2[imgNum2 + j][i];
				diff += Math.abs(sample1 - sample2);
			}*/
			
			for (i = 0; i < COEFFICIENT_NUM; i++) {
				//System.out.println("i: " + i + " " + j);
				diff += Math.abs(this.fftsin[imgNum1 + j][i] - af2.fftsin[imgNum2 + j][i]);
				diff += Math.abs(this.fftcos[imgNum1 + j][i] - af2.fftcos[imgNum2 + j][i]);
				diff += Math.abs(this.fftsin2[imgNum1 + j][i] - af2.fftsin2[imgNum2 + j][i]);
				diff += Math.abs(this.fftcos2[imgNum1 + j][i] - af2.fftcos2[imgNum2 + j][i]);
			}
		}
		
		
		return diff;
	}
	
}
