package audioProcessor;

import java.util.ArrayList;

public abstract class AudioFeature {
	double weight;
	String featureName;
	
	public void getFeatureFromeAudioData(ArrayList<Byte> sample1, ArrayList<Byte> sample2, int channelNum, int frameSize, int imageNum) {}
	public abstract double featureDiff(AudioFeature af, int imgNum1, int imgNum2, int frameNum);
}
