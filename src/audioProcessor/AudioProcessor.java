package audioProcessor;
import java.awt.List;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.DataLine.Info;

import multiMediaQuerySystem.ClipInfo;

import org.wikijava.sound.playWave.*;

import tools.DataTypeHelper;

import com.sun.media.sound.WaveFileReader;

import java.io.*;


public class AudioProcessor {
	public static final int EXTERNAL_BUFFER_SIZE = 524288;
	
	public SourceDataLine getAudioSteamFromFile(String filename) throws PlayWaveException
	{
		// opens the inputStream
		FileInputStream inputStream = null;
		AudioInputStream audioInputStream = null;
		
		try {
		    inputStream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    return null;
		}
		
		BufferedInputStream bufferedIn = new BufferedInputStream(inputStream);
		try {
			audioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Obtain the information about the AudioInputStream
		AudioFormat audioFormat = audioInputStream.getFormat();
		Info info = new Info(SourceDataLine.class, audioFormat);

		// opens the audio channel
		SourceDataLine dataLine = null;
		try {
		    dataLine = (SourceDataLine) AudioSystem.getLine(info);
		    dataLine.open(audioFormat, this.EXTERNAL_BUFFER_SIZE);
		} catch (LineUnavailableException e1) {
		    throw new PlayWaveException(e1);
		}
		
		return dataLine;
	}
	
	public void readAudioFileAndRecordFeatures(String filename, int imageNum, ClipInfo clip) throws IOException {
		System.out.println("Reading audio" + filename);
		FileInputStream inputStream = null;
		AudioInputStream audioInputStream = null;
		
		try {
		    inputStream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		}
		
		
		BufferedInputStream bufferedIn = new BufferedInputStream(inputStream);
		try {
			audioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Obtain the information about the AudioInputStream
		AudioFormat audioFormat = audioInputStream.getFormat();
		int readBytes = 0;
		int frameSize = audioFormat.getFrameSize();
		int sampleBits = audioFormat.getSampleSizeInBits();
		int channelNum = audioFormat.getChannels();	// 2 for stereo, 16bits
		int count = 0;
		
		byte[] audioBuffer = new byte[frameSize];
		ArrayList<Byte> sampleData1 = new ArrayList<Byte>();
		ArrayList<Byte> sampleData2 = new ArrayList<Byte>();
		
		do {
			readBytes = audioInputStream.read(audioBuffer, 0, audioBuffer.length);
			
			if (readBytes > 0 && channelNum == 2) {
				//System.out.println("read bytes: " + readBytes);
				sampleData1.add(audioBuffer[0]);
				sampleData1.add(audioBuffer[1]);
				sampleData2.add(audioBuffer[0]);
				sampleData2.add(audioBuffer[1]);
			}
		} while (readBytes >= frameSize);
		
		clip.audioFFT.getFeatureFromeAudioData(sampleData1, sampleData2, channelNum, frameSize, imageNum);
		clip.audioFileName = filename;
		
		System.out.println("Finish reading audio" + filename);
	}
	
	public void queryAudioFromFile(String filename) {
		
	}
	
	private double getAudioDiffValue(AudioInfo audio1, AudioInfo audio2)
	{
		double distance = 0;
		
		
		return distance;
	}
	
	public static double[] getAudioData(String filename) throws IOException
	{
		FileInputStream inputStream = null;
		AudioInputStream audioInputStream = null;
		
		try {
		    inputStream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		}
		
		
		BufferedInputStream bufferedIn = new BufferedInputStream(inputStream);
		try {
			audioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Obtain the information about the AudioInputStream
		AudioFormat audioFormat = audioInputStream.getFormat();
		int readBytes = 0;
		int frameSize = audioFormat.getFrameSize();
		int sampleBits = audioFormat.getSampleSizeInBits();
		int channelNum = audioFormat.getChannels();	// 2 for stereo, 16bits
		int count = 0;
		
		byte[] audioBuffer = new byte[frameSize];
		ArrayList<Byte> sampleData1 = new ArrayList<Byte>();
		do {
			readBytes = audioInputStream.read(audioBuffer, 0, audioBuffer.length);
			
			if (readBytes > 0 && channelNum == 2) {
				//System.out.println("read bytes: " + readBytes);
				sampleData1.add(audioBuffer[0]);
				sampleData1.add(audioBuffer[1]);
			}
		} while (readBytes >= frameSize);
		
		int temp;
		int sampleSize = frameSize / channelNum;
		int length = sampleData1.size() / sampleSize;
		double sample[] = new double[length];
		byte high, low;
		temp = 0;
		for (int i = 0; i < length; i++) {
			high = sampleData1.get(temp);
			temp++;
			low = sampleData1.get(temp);
			temp++;
			sample[i] = tools.DataTypeHelper.twoBytesToInt(high, low);
			sample[i] /= 32768;
			sample[i] += 1;
		}
		return sample;
	}
}
