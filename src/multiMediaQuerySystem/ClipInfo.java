package multiMediaQuerySystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import com.sun.istack.internal.FragmentContentHandler;

import sun.net.www.content.text.plain;
import vedioProcessor.ImageMainColor;
import vedioProcessor.ImageORB;
import vedioProcessor.MotionVector;
import audioProcessor.*;

public class ClipInfo {
	public String clipName;
	public String audioFileName;
	public int imageNum;
	public String imageFileNames[];
	
	public AudioFFTFeature audioFFT;
	public ImageMainColor mainColor[];
	public MotionVector motionVector[];
	public ImageORB orbs[];
	
	public double similarity[];
	
	public ClipInfo() 
	{
		this.audioFFT = new AudioFFTFeature();
		this.similarity = new double[600];
	}
	
	public void initImageFeatures(int imageNum)
	{
		this.imageNum = imageNum;
		//this.imageFileNames = new String[imageNum];
		this.mainColor = new ImageMainColor[imageNum];
		this.motionVector = new MotionVector[imageNum];
		this.orbs = new ImageORB[imageNum];
		
		for (int i = 0; i < imageNum; i++) {
			this.mainColor[i] = new ImageMainColor();
			this.motionVector[i] = new MotionVector();
			this.orbs[i] = new ImageORB();
		}
	}
	
	public void setImageFileNames(String filenames[]) {
		this.imageFileNames = new String[filenames.length];
		int i = 0;
		for (i = 0; i < filenames.length; i++) {
			imageFileNames[i] = filenames[i];
		}
	}
	
	public void recordDataToFile(String filename) throws IOException {
		
		filename = filename + clipName + ".clipinfo";
		FileOutputStream out = new FileOutputStream(filename);
		PrintStream p=new PrintStream(out);
		
		int i, j, k, l;
		
		for (i = 0; i < imageNum; i++) {
			for (j = 0; j < AudioFFTFeature.SUBSAMPLE_FOR_IMAGE; j++) {
				p.println(this.audioFFT.subsampleData[i][j]);
				p.println(this.audioFFT.subsampleData2[i][j]);
			}
		}
		
		for (i = 0; i < imageNum; i++) {
			for (j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
				p.println(this.audioFFT.fftsin[i][j]);
				p.println(this.audioFFT.fftcos[i][j]);
				p.println(this.audioFFT.fftsin2[i][j]);
				p.println(this.audioFFT.fftcos2[i][j]);
			}
		}
		
		for (l = 0; l < imageNum; l++) {
			for (i = 0; i < ImageMainColor.H_LEVEL_NUM; i++) {
				for (j = 0; j < ImageMainColor.S_LEVEL_NUM; j++) {
					for (k = 0; k < ImageMainColor.V_LEVEL_NUM; k++) {
						p.println(this.mainColor[l].HSV_Count[i][j][k]);
					}
				}
			}
			
			for (i = 0; i < 9; i++) {
				p.println(this.motionVector[l].motions[i]);
			}
		}
		
		System.out.println("output fft complete " + clipName);
	}
	
	public void readDataFromFile(String filename) throws IOException {
		filename = filename + clipName + ".clipinfo";
		Scanner input = new Scanner(new File(filename));
		
		this.initImageFeatures(600);
		
		int i, j, l, k;
		String str;
		
		this.audioFFT.subsampleData = new short[imageNum][AudioFFTFeature.SUBSAMPLE_FOR_IMAGE];
		this.audioFFT.subsampleData2 = new short[imageNum][AudioFFTFeature.SUBSAMPLE_FOR_IMAGE];
		
		for (i = 0; i < imageNum; i++) {
			for (j = 0; j < AudioFFTFeature.SUBSAMPLE_FOR_IMAGE; j++) {
				str = input.nextLine();
				this.audioFFT.subsampleData[i][j] = Short.parseShort(str);
				str = input.nextLine();
				this.audioFFT.subsampleData2[i][j] = Short.parseShort(str);
			}
		}
		
		this.audioFFT.fftsin = new double[imageNum][AudioFFTFeature.COEFFICIENT_NUM];
		this.audioFFT.fftsin2 = new double[imageNum][AudioFFTFeature.COEFFICIENT_NUM];
		this.audioFFT.fftcos = new double[imageNum][AudioFFTFeature.COEFFICIENT_NUM];
		this.audioFFT.fftcos2 = new double[imageNum][AudioFFTFeature.COEFFICIENT_NUM];
		
		for (i = 0; i < imageNum; i++) {
			for (j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
				str = input.nextLine();
				this.audioFFT.fftsin[i][j] = Double.parseDouble(str);
				str = input.nextLine();
				this.audioFFT.fftcos[i][j] = Double.parseDouble(str);
				str = input.nextLine();
				this.audioFFT.fftsin2[i][j] = Double.parseDouble(str);
				str = input.nextLine();
				this.audioFFT.fftcos2[i][j] = Double.parseDouble(str);
			}
		}
		
		this.mainColor = new ImageMainColor[imageNum];
		this.motionVector = new MotionVector[imageNum];
		
		for (l = 0; l < imageNum; l++) {
			this.mainColor[l] = new ImageMainColor();
			this.mainColor[l].HSV_Count = new double[ImageMainColor.H_LEVEL_NUM][ImageMainColor.S_LEVEL_NUM][ImageMainColor.V_LEVEL_NUM];
			for (i = 0; i < ImageMainColor.H_LEVEL_NUM; i++) {
				for (j = 0; j < ImageMainColor.S_LEVEL_NUM; j++) {
					for (k = 0; k < ImageMainColor.V_LEVEL_NUM; k++) {
						str = input.nextLine();
						this.mainColor[l].HSV_Count[i][j][k] = Double.parseDouble(str);
					}
				}
			}
			
			this.motionVector[l] = new MotionVector();
			this.motionVector[l].motions = new int[9];
			for (i = 0; i < 9; i++) {
				str = input.nextLine();
				this.motionVector[l].motions[i] = Integer.parseInt(str);
			}
		}
		
		System.out.println("input complete " + clipName);
	}
}
