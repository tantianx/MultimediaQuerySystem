package multiMediaQuerySystem;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.media.jai.Histogram;
import javax.sound.sampled.LineUnavailableException;

import org.wikijava.sound.playWave.PlayWaveException;

import vedioProcessor.VedioProcessor;
import systemUI.*;
import tools.MatchRecord;
import audioProcessor.*;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class MultiMediaQueryApplication {

	static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
	
	public static ClipInfo databaseClips[];
	public static ClipInfo queryClip;
	public static String databaseFolderName = "./db/";
	public static String catagoryNames[] = {"flowers", "interview", "movie", "musicvideo", "sports", "starcraft", "traffic"};
	public static int catagoryNum = catagoryNames.length;
	public static ApplicationFrame systemFrame;
	
	public static void main(String[] args) throws IOException, PlayWaveException, LineUnavailableException {

		String folderName, audioName;
		String imageNames[];
		
		databaseClips = new ClipInfo[7];
		for (int i = 0; i < 7; i++) {
			databaseClips[i] = new ClipInfo();
			databaseClips[i].clipName = catagoryNames[i];
		}
		
		systemFrame = new ApplicationFrame();
		
		for (int i = 0; i < 7; i++) {
			
			folderName = databaseFolderName + catagoryNames[i] + "/";
			audioName = tools.FileNamesReader.getAudioFileName(folderName);
			imageNames = tools.FileNamesReader.getImageFileNames(folderName);
			
			databaseClips[i].imageFileNames = new String[600];
			for (int j = 0; j < 600; j++) {
				databaseClips[i].imageFileNames[j] = imageNames[j];
			}
			databaseClips[i].audioFileName = audioName;
			
			AudioProcessor ap = new AudioProcessor();
			ap.readAudioFileAndRecordFeatures(audioName, 600, databaseClips[i]);
			
			VedioProcessor vp = new VedioProcessor();
			vp.getImageFeaturesFromFiles(imageNames, databaseClips[i]);
			
			System.out.println("finish " + i);
			databaseClips[i].recordDataToFile(databaseFolderName);
			
			//databaseClips[i].imageNum = 600;
			//databaseClips[i].readDataFromFile(databaseFolderName);
			
		}
		//databaseClips[0].recordDataToFile(databaseFolderName);
		System.out.println("Read in all the data.");
	}
	
	public static void query(String imgNames[]) throws IOException, LineUnavailableException {
		MatchRecord.init();
		
		queryClip.initImageFeatures(imgNames.length);
		queryClip.setImageFileNames(imgNames);
		AudioProcessor ap = new AudioProcessor();
		ap.readAudioFileAndRecordFeatures(queryClip.audioFileName, queryClip.imageNum, queryClip);
		
		VedioProcessor vp = new VedioProcessor();
		vp.getImageFeaturesFromFiles(queryClip.imageFileNames, queryClip);
		
		for (int i = 0; i < 7; i++) {			
			System.out.println("clip: " + i);
			
			for (int j = 0; j < databaseClips[i].imageNum - queryClip.imageNum; j++) {
				double diffC, diffM;
				diffC = diffM = 0;
				MatchRecord.audioDiff[i][j] = queryClip.audioFFT.featureDiff(databaseClips[i].audioFFT, 0, j, queryClip.imageNum - AudioFFTFeature.IMAGE_COUNT_PER_TRANSFORM);
				for (int k = 0; k < queryClip.imageNum; k++) {
					diffM += queryClip.motionVector[k].featureDiff(databaseClips[i].motionVector[j + k]);
					diffC += queryClip.mainColor[k].featureDiff(databaseClips[i].mainColor[j + k]);
				}
				MatchRecord.colorDiff[i][j] = diffC;
				MatchRecord.motionDiff[i][j] = diffM;
			}
		}
		MatchRecord.normalize();
		tools.MatchRecord.calculateLargeCount();
		
		int rank[] = new int[2];
		tools.MatchRecord.getDiffAt(1, rank);
		
		System.out.println("rank1: " + rank[0] + " " + rank[1]);
		
		String matchImages[] = new String[150];
		for (int i = 0; i < 150; i++) {
			matchImages[i] = databaseClips[rank[0]].imageFileNames[i + rank[1]];
		}
		
		systemFrame.setupQueryResult(queryClip.imageFileNames, queryClip.audioFileName, 
				matchImages, databaseClips[rank[0]].audioFileName, rank[1],  rank[0]);
		
		System.out.println("query end");
	}
}
