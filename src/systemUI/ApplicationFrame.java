package systemUI;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.media.jai.Histogram;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import systemUI.VedioThread;
import multiMediaQuerySystem.ClipInfo;
import multiMediaQuerySystem.MultiMediaQueryApplication;
import audioProcessor.AudioFFTFeature;
import audioProcessor.AudioProcessor;
import vedioProcessor.VedioProcessor;
import tools.*;

public class ApplicationFrame extends JFrame {
	
	public static final long serialVersionUID = 1L;
	
	public JLabel queryVedio;
	public JLabel resultVedio;
	
	public JLabel queryWavImg;
	public JLabel resultWavImg;
	public JLabel queryWavImg2;
	public JLabel resultWavImg2;
	
	public JLabel queryMainColorImg;
	public JLabel resultMainColorImg;
	
	public JLabel queryMotionImg;
	public JLabel resultMotionImg;
	
	public JList summaryRankList;
	public JList summaryNameList;
	public JList summaryPointList;
	public JList summaryAudioDiff;
	public JList summaryColorDiff;
	public JList summaryMotionDiff;
	
	public JPanel summaryPanel;
	
	public DefaultListModel<String> rankList;
	public DefaultListModel<String> nameList;
	public DefaultListModel<String> pointList;
	public DefaultListModel<String> audioDiffList;
	public DefaultListModel<String> colorDiffList;
	public DefaultListModel<String> motionDiffList;
	
	public JTable summaryTable;
	
	public String matchPlayList[];
	public String queryPlayList[];
	public String queryAudioFileName;
	public String matchAudioFileName;
	
	public AudioInputStream resultAudioInputStream;
	public int currentFrame;
	public int audioPlayBytesPerTime;
	public PlayerStatus playerStatus;
	public JSlider slider;
	public VedioProcessor vedioProcessor;
	public Clip resultClip;
	public int clipFrame;
	public int clipFramePerImage;
	public BufferedImage nextImg;
	public boolean setNextImg;
	
	private audioThread audioThread;
	private VedioThread playThread;
	public int matchAudioOffset;
	
	public int offset;
	public boolean isPlayingMatchSound;
	
	private ApplicationFrame frame;
	public int databaseSub;
	public boolean isPlayingMatchAudio;
	
	public ApplicationFrame() throws IOException
	{
		this.initFrame();
		this.playerStatus = PlayerStatus.PAUSE;
		vedioProcessor = new VedioProcessor();
		playThread = null;
		audioThread = null;
		frame = this;
		this.matchAudioOffset = 0;
		isPlayingMatchAudio = false;
	}
	
	private void initFrame() throws IOException
	{
		VedioProcessor imageProcessor = new VedioProcessor();
		BufferedImage img = imageProcessor.getImageBufferedImageFromFile("/Users/mac/Desktop/576/group/MultimediaQuerySystem/db/HQ2/HQ2_001.rgb");
		
		queryVedio =  this.createImageTextLabel("Query", new ImageIcon(img));
		resultVedio = this.createImageTextLabel("Match", new ImageIcon(img));
		
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout(new BoxLayout(mainContainer, BoxLayout.X_AXIS));
		mainContainer.setLayout(new FlowLayout());
		
		JPanel playerContainer = new JPanel();
		playerContainer.setLayout(new BoxLayout(playerContainer, BoxLayout.Y_AXIS));
		playerContainer.setBorder(new EmptyBorder(0, 0, 0, 0));
		
		JPanel buttonGroup = new JPanel();
		buttonGroup.setLayout(new FlowLayout());
		buttonGroup.setBorder(new EmptyBorder(0, 0, 0, 0));
		
		JButton playButton = new JButton("PLAY");
		JButton pauseButton = new JButton("PAUSE");
		JButton stopButton = new JButton("STOP");
		
		playButton.addActionListener(new playHandler());
		pauseButton.addActionListener(new pauseHandler());
		stopButton.addActionListener(new stopHandler());
		
		buttonGroup.add(playButton);
		buttonGroup.add(Box.createHorizontalStrut(3));
		buttonGroup.add(pauseButton);
		buttonGroup.add(Box.createHorizontalStrut(3));
		buttonGroup.add(stopButton);
		
		slider = new JSlider(0, 100);
		slider.setMaximum(100);
		slider.setMinimum(0);
		slider.setValue(0);
		slider.addChangeListener(new sliderHandler());
		
		JPanel sliderPanel = new JPanel();
		sliderPanel.setLayout(new FlowLayout());
		sliderPanel.add(slider);
		
		playerContainer.add(includeInFlowLayoutPanel(queryVedio));
		playerContainer.add(Box.createVerticalStrut(10));
		playerContainer.add(includeInFlowLayoutPanel(resultVedio));
		playerContainer.add(Box.createVerticalStrut(10));
		playerContainer.add(sliderPanel);
		playerContainer.add(buttonGroup);
		
		ScrollPane playerScroll = new ScrollPane();
		playerScroll.add(playerContainer);
		playerScroll.setSize(400, 650);
		
		mainContainer.add(playerScroll);
		
		// Show the features.
		JPanel featureBoard = new JPanel();
		featureBoard.setLayout(new BoxLayout(featureBoard, BoxLayout.Y_AXIS));
			
		double data[] = AudioProcessor.getAudioData("/Users/mac/Desktop/temp.wav");
		ImageIcon imgd = Hitogram.createWaveGraph(350, 144, data);
		
		this.queryWavImg = this.createImageTextLabel("fft sin 1", imgd);	
		this.resultWavImg = this.createImageTextLabel("fft cos 1", imgd);
		this.queryWavImg2 = this.createImageTextLabel("fft sin 2", imgd);
		this.resultWavImg2 = this.createImageTextLabel("fft cos 2", imgd);
		JPanel feature1 = new JPanel();
		feature1.setLayout(new FlowLayout());
		
		double data1[] = {20, 40, 60, 80};
		double data2[] = {20, 40, 60, 80};
		
		ImageIcon img2 = Hitogram.createHSVHitogram(700, 144, data1, data2);
		
		//feature1.add(createImageTextLabel("HSV-Query", img2));
		
		this.queryMainColorImg = this.createImageTextLabel("Main Color", imgd);
		//this.resultMainColorImg = this.createImageTextLabel("Match Main Color", imgd);
		
		this.queryMotionImg = this.createImageTextLabel("Motion Vector", imgd);
		//this.resultMotionImg = this.createImageTextLabel("Query Motion Vector", imgd);
		
		JPanel feature2 = new JPanel();
		feature2.setLayout(new FlowLayout());
		
		JPanel feature3 = new JPanel();
		feature3.setLayout(new FlowLayout());
		
		JPanel feature12 = new JPanel();
		feature12.setLayout(new FlowLayout());
		
		feature12.add(this.queryWavImg2);
		feature12.add(this.resultWavImg2);
		feature1.add(queryWavImg);
		feature1.add(resultWavImg);
		
		feature2.add(this.queryMainColorImg);
		//feature2.add(this.resultMainColorImg);
		
		feature3.add(this.queryMotionImg);
		//feature3.add(this.resultMotionImg);
		
		summaryPanel = new JPanel();
		summaryPanel.setLayout(new FlowLayout());
		
		this.rankList = new DefaultListModel<String>();
		this.nameList = new DefaultListModel<String>();
		this.pointList = new DefaultListModel<String>();
		this.audioDiffList = new DefaultListModel<String>();
		this.colorDiffList = new DefaultListModel<String>();
		this.motionDiffList = new DefaultListModel<String>();
		
		this.rankList.addElement("hello");
		this.nameList.addElement("hello");
		this.pointList.addElement("hello");
		
		this.summaryRankList = new JList(rankList);
		this.summaryPointList = new JList(pointList);
		this.summaryNameList = new JList(nameList);
		this.summaryAudioDiff = new JList(audioDiffList);
		this.summaryColorDiff = new JList(colorDiffList);
		this.summaryMotionDiff = new JList(motionDiffList);
		
		this.summaryRankList.setFixedCellWidth(40);
		this.summaryNameList.setFixedCellWidth(100);
		this.summaryPointList.setFixedCellWidth(100);
		this.summaryAudioDiff.setFixedCellWidth(100);
		this.summaryColorDiff.setFixedCellWidth(100);
		this.summaryMotionDiff.setFixedCellWidth(100);
		
		summaryPanel.add(summaryRankList);
		summaryPanel.add(summaryNameList);
		summaryPanel.add(summaryPointList);
		summaryPanel.add(summaryAudioDiff);
		summaryPanel.add(summaryColorDiff);
		summaryPanel.add(summaryMotionDiff);
		
		featureBoard.add(feature1);
		featureBoard.add(feature12);
		featureBoard.add(feature2);
		featureBoard.add(feature3);
		featureBoard.add(summaryPanel);
		ScrollPane featureBoardScroll = new ScrollPane();
		featureBoardScroll.add(featureBoard);
		featureBoardScroll.setSize(750, 650);
		mainContainer.add(featureBoardScroll);
		
		// Add Menu bar.
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		
		JMenuItem queryItem = new JMenuItem("Query");
		JMenuItem updateItem = new JMenuItem("Update");
		JMenuItem jumpItem = new JMenuItem("Jump To");
		JMenuItem switchSoundItem = new JMenuItem("Switch Sound");
		
		queryItem.addActionListener(new queryItemHandler());
		updateItem.addActionListener(new updateItemHandler());
		jumpItem.addActionListener(new jumpToItemHandler());
		switchSoundItem.addActionListener(new switchSoundItemHandler());
		
		fileMenu.add(jumpItem);
		fileMenu.add(queryItem);
		fileMenu.add(updateItem);
		fileMenu.add(switchSoundItem);
		menuBar.add(fileMenu);
		
		this.setJMenuBar(menuBar);
		
		this.add(mainContainer);
		this.setTitle("Multimedia Query System");
		this.setSize(1200, 700);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private JLabel createImageTextLabel(String text, ImageIcon img)
	{
		JLabel label = new JLabel(img);
		label.setVerticalTextPosition(JLabel.BOTTOM);
		label.setHorizontalTextPosition(JLabel.CENTER);
		label.setText(text);
		return label;
	}
	
	private JPanel includeInFlowLayoutPanel(JLabel comp)
	{
		JPanel temp = new JPanel();
		temp.setLayout(new FlowLayout());
		temp.add(comp);
		return temp;
	}
	
	public void showAudioFeature() {
		double[] result;   
		double[] query;
		
		result = new double[AudioFFTFeature.COEFFICIENT_NUM];
		query = new double[AudioFFTFeature.COEFFICIENT_NUM];
		int start = this.matchAudioOffset;
		int count = 0;
		double min = Double.MAX_VALUE;
		
		for (int j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
			result[count] = MultiMediaQueryApplication.databaseClips[this.databaseSub].audioFFT.fftcos[this.currentFrame + start][j];
			query[count] = MultiMediaQueryApplication.queryClip.audioFFT.fftcos[this.currentFrame][j];
			min = min > result[count] ? result[count] : min;
			min = min > query[count] ? query[count] : min;
			count++;
		}
		for (int i = 0; i < AudioFFTFeature.COEFFICIENT_NUM; i++) {
			result[i] -= min;
			query[i] -= min;
		}
		ImageIcon img1 = Hitogram.createHSVHitogram(350, 144, result, query);
		this.resultWavImg.setIcon(img1);
		
		count = 0;
		for (int j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
			result[count] = MultiMediaQueryApplication.databaseClips[this.databaseSub].audioFFT.fftcos2[this.currentFrame + start][j];
			query[count] = MultiMediaQueryApplication.queryClip.audioFFT.fftcos2[this.currentFrame][j];
			min = min > result[count] ? result[count] : min;
			min = min > query[count] ? query[count] : min;
			count++;
		}
		for (int i = 0; i < AudioFFTFeature.COEFFICIENT_NUM; i++) {
			result[i] -= min;
			query[i] -= min;
		}
		img1 = Hitogram.createHSVHitogram(350, 144, result, query);
		this.resultWavImg2.setIcon(img1);
		
		count = 0;
		for (int j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
			result[count] = MultiMediaQueryApplication.databaseClips[this.databaseSub].audioFFT.fftsin[this.currentFrame + start][j];
			query[count] = MultiMediaQueryApplication.queryClip.audioFFT.fftsin[this.currentFrame][j];
			min = min > result[count] ? result[count] : min;
			min = min > query[count] ? query[count] : min;
			count++;
		}
		for (int i = 0; i < AudioFFTFeature.COEFFICIENT_NUM; i++) {
			result[i] -= min;
			query[i] -= min;
		}
		img1 = Hitogram.createHSVHitogram(350, 144, result, query);
		this.queryWavImg.setIcon(img1);
		
		count = 0;
		for (int j = 0; j < AudioFFTFeature.COEFFICIENT_NUM; j++) {
			result[count] = MultiMediaQueryApplication.databaseClips[this.databaseSub].audioFFT.fftsin2[this.currentFrame + start][j];
			query[count] = MultiMediaQueryApplication.queryClip.audioFFT.fftsin2[this.currentFrame][j];
			min = min > result[count] ? result[count] : min;
			min = min > query[count] ? query[count] : min;
			count++;
		}
		for (int i = 0; i < AudioFFTFeature.COEFFICIENT_NUM; i++) {
			result[i] -= min;
			query[i] -= min;
		}
		img1 = Hitogram.createHSVHitogram(350, 144, result, query);
		this.queryWavImg2.setIcon(img1);
		
	}
	
	public void showMainColorFeature() {
		int databaseSub = this.currentFrame + this.offset;
		int querySub = this.currentFrame;
		
		double mainColorDatabase[], mainColorQuery[];
		int len = 8 * 4 * 4;
		
		mainColorDatabase = new double[len];
		mainColorQuery = new double[len];
		
		int i, j, k, t;
		t = 0;
		for (i = 0; i < 8; i++) {
			for (j = 0; j < 4; j++) {
				for (k = 0; k < 4; k++) {
					mainColorDatabase[t] = MultiMediaQueryApplication.databaseClips[this.databaseSub].mainColor[databaseSub].HSV_Count[i][j][k];
					mainColorQuery[t] = MultiMediaQueryApplication.queryClip.mainColor[querySub].HSV_Count[i][j][k];
					t++;
				}
			}
		}
		
		ImageIcon img = Hitogram.createHSVHitogram(700, 288, mainColorDatabase, mainColorQuery);
		this.queryMainColorImg.setIcon(img);
	}
	
	public void showMotionFeature() {
		int databaseSub = this.currentFrame + this.offset;
		int querySub = this.currentFrame;
		
		double motionsDatabase[], motionQuery[];
		int i = 0;
		
		motionQuery = new double[9];
		motionsDatabase = new double[9];
		
		for (i = 0; i < 9; i++) {
			motionQuery[i] = MultiMediaQueryApplication.queryClip.motionVector[querySub].motions[i];
			motionsDatabase[i] = MultiMediaQueryApplication.databaseClips[this.databaseSub].
					motionVector[databaseSub].motions[i];
		}
		
		ImageIcon img = Hitogram.createHSVHitogram(700, 288, motionsDatabase, motionQuery);
		this.queryMotionImg.setIcon(img);
	}
	
	public void setupQueryResult(String queryVedio[], String queryAudio, String matchVedio[], String matchAudio, 
			int matchAudioStartFrame, int database) throws LineUnavailableException 
	{
		
		this.queryPlayList = queryVedio;
		this.queryAudioFileName = queryAudio;
		this.matchPlayList = matchVedio;
		this.matchAudioFileName = matchAudio;
		
		this.setPlayList(matchPlayList);
		this.setAudioFromFile(matchAudio);
		this.databaseSub = database;
		this.matchAudioOffset = matchAudioStartFrame;
		
		this.showAudioFeature();
		this.showMainColorFeature();
		this.showMotionFeature();
		
		this.isPlayingMatchAudio = true;
		this.offset = matchAudioStartFrame;
		
		playerStatus = playerStatus.PAUSE;
		currentFrame = 0;
		BufferedImage img = vedioProcessor.getImageBufferedImageFromFile(matchPlayList[currentFrame]);
		resultVedio.setIcon(new ImageIcon(img));
		slider.setValue(0);
		resultClip.stop();
		if (playThread != null) playThread.stop();
		if (audioThread != null) audioThread.stop();
	}
	
	public void setPlayList(String list[])
	{
		this.matchPlayList = new String[list.length];
		for (int i = 0; i < list.length; i++)
			this.matchPlayList[i] = list[i];
		this.currentFrame = 0;
		slider.setMaximum(matchPlayList.length - 1);
		slider.setMinimum(this.currentFrame);
	}
	
	public void setAudioFromFile(String filename) throws LineUnavailableException
	{
		FileInputStream inputStream = null;
		
		try {
			inputStream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		InputStream bufferedIn = new BufferedInputStream(inputStream);
		try {
			this.resultAudioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
			AudioFormat format = this.resultAudioInputStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, format);

			this.resultClip = (Clip) AudioSystem.getLine(info);
			this.resultClip.open(this.resultAudioInputStream);
			
			int len = 600;
			this.clipFramePerImage = resultClip.getFrameLength() / len;
			this.clipFrame = 0;

		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	class playHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			playerStatus = PlayerStatus.PLAYING;
			resultClip.setFramePosition((currentFrame + frame.matchAudioOffset) * clipFramePerImage);
			playThread = new VedioThread(frame);
			audioThread = new audioThread(frame);
			audioThread.start();
			playThread.start();
		}
		
	}
	
	class pauseHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			playerStatus = PlayerStatus.PAUSE;
			resultClip.stop();
			if (playThread != null) playThread.stop();
			if (audioThread != null) audioThread.stop();
		}
		
	}
	
	class stopHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			playerStatus = playerStatus.PAUSE;
			currentFrame = 0;
			BufferedImage img = vedioProcessor.getImageBufferedImageFromFile(matchPlayList[currentFrame]);
			resultVedio.setIcon(new ImageIcon(img));
			slider.setValue(0);
			resultClip.stop();
			if (playThread != null) playThread.stop();
			if (audioThread != null) audioThread.stop();
		}
		
	}
	
	class sliderHandler implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent arg0) {
			
			currentFrame = slider.getValue();
			
			BufferedImage img = vedioProcessor.getImageBufferedImageFromFile(matchPlayList[currentFrame]);
			resultVedio.setIcon(new ImageIcon(img));
			
			BufferedImage img2 = vedioProcessor.getImageBufferedImageFromFile(queryPlayList[currentFrame]);
			queryVedio.setIcon(new ImageIcon(img2));
	
			if (frame.playerStatus == PlayerStatus.PAUSE) {
				frame.showMainColorFeature();
				frame.showMotionFeature();
				frame.showAudioFeature();
			}
		}
		
	}
	
	class queryItemHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JFileChooser fc = new JFileChooser("/Users/mac/Desktop/576/group/MultimediaQuerySystem/db");
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
			fc.showDialog(null,null);
			File queryDir = fc.getSelectedFile();
			System.out.println(queryDir.getPath());
			String queryPath = queryDir.getPath();
			
			MultiMediaQueryApplication.queryClip = new ClipInfo();
			MultiMediaQueryApplication.queryClip.audioFileName = tools.FileNamesReader.getAudioFileName(queryPath);
			String tempNames[] = tools.FileNamesReader.getImageFileNames(queryPath);
			
			try {
				MultiMediaQueryApplication.query(tempNames);
			} catch (IOException | LineUnavailableException e1) {
				e1.printStackTrace();
			}
			
			String ranks[], names[], points[], adiff[], cdiff[], mdiff[];
			ranks = new String[51];
			names = new String[51];
			points = new String[51];
			adiff = new String[51];
			cdiff = new String[51];
			mdiff = new String[51];
			
			MatchRecord.generateTableContent(ranks, names, points, adiff, cdiff, mdiff);
			frame.rankList.clear();
			frame.nameList.clear();
			frame.pointList.clear();
			frame.audioDiffList.clear();
			frame.colorDiffList.clear();
			frame.motionDiffList.clear();
			
			for (int i = 0; i < ranks.length; i++) {
				frame.rankList.addElement(ranks[i]);
				frame.nameList.addElement(names[i]);
				frame.pointList.addElement(points[i]);
				frame.audioDiffList.addElement(adiff[i]);
				frame.colorDiffList.addElement(cdiff[i]);
				frame.motionDiffList.addElement(mdiff[i]);
			}
			
			frame.setVisible(true);
		}
	}
	
	class updateItemHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.currentFrame = frame.currentFrame >= frame.queryPlayList.length ? frame.queryPlayList.length - 1 : frame.currentFrame; 
			frame.showMainColorFeature();
			frame.showMotionFeature();
		}

	}
	
	class jumpToItemHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String inputValue = JOptionPane.showInputDialog("Please input a value");
			String data[] = inputValue.split(" ");
			String databaseName = data[0];
			int startNum = Integer.parseInt(data[1]);
			
			int database = 0;
			for (int i = 0; i < MultiMediaQueryApplication.catagoryNum; i++) {
				if (databaseName.compareTo(MultiMediaQueryApplication.catagoryNames[i]) == 0) {
					database = i;
					break;
				}
			}
			
			String newPlayList[] = new String[frame.queryPlayList.length];
			
			for (int i = 0; i < frame.queryPlayList.length; i++)
				newPlayList[i] = MultiMediaQueryApplication.databaseClips[database].imageFileNames[i + startNum];
			
			try {
				frame.setupQueryResult(frame.queryPlayList, frame.queryAudioFileName, 
						newPlayList, MultiMediaQueryApplication.databaseClips[database].audioFileName, startNum, database);
			} catch (LineUnavailableException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	
	class switchSoundItemHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (frame.isPlayingMatchAudio) {
				frame.matchAudioOffset = 0;
				frame.isPlayingMatchAudio = false;
				try {
					frame.setAudioFromFile(frame.queryAudioFileName);
				} catch (LineUnavailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else {
				frame.matchAudioOffset = frame.offset;
				frame.isPlayingMatchAudio = true;
				try {
					frame.setAudioFromFile(frame.matchAudioFileName);
				} catch (LineUnavailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		 
	}
}
