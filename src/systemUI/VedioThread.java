package systemUI;

import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

public class VedioThread extends Thread {
	private ApplicationFrame frame;
	
	public VedioThread(ApplicationFrame applicationFrame) {
		this.frame = applicationFrame;
	}
	
	@Override
    public void run() {
		while(frame.currentFrame <= frame.matchPlayList.length) {
			if (frame.playerStatus == PlayerStatus.PLAYING)
			{
				if (frame.currentFrame < frame.matchPlayList.length)
				{
					BufferedImage img = frame.vedioProcessor.getImageBufferedImageFromFile(
							frame.matchPlayList[frame.currentFrame]);
					frame.resultVedio.setIcon(new ImageIcon(img));
					frame.slider.setValue(frame.currentFrame);
					System.out.println("output image clip: " + frame.resultClip.getFramePosition());
					
					while(frame.resultClip.getFramePosition() < frame.clipFramePerImage * (frame.currentFrame + frame.matchAudioOffset) + frame.clipFramePerImage)
					{
						// Do nothing.
					}
					
					frame.currentFrame++;
				}
				else 
				{
					frame.resultClip.stop();
					frame.playerStatus = PlayerStatus.PAUSE;
				}
			}
		}
    }
}
