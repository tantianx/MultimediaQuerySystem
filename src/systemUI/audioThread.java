package systemUI;

public class audioThread extends Thread{
	private ApplicationFrame frame;
	
	public audioThread(ApplicationFrame applicationFrame) {
		this.frame = applicationFrame;
	}
	
	@Override
    public void run() {
		if (!frame.resultClip.isRunning())
			frame.resultClip.start();
	}
}
