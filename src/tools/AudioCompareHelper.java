package tools;

import audioProcessor.AudioFFTFeature;

public class AudioCompareHelper {
	public static int[] database;
	public static int[] query;
	/*
	public static void rebuildDatabase(AudioFFTFeature fftFeature) {
		Complex y[] = new Complex[fftFeature.originalSize];
		
		int i, j, intervals, count;
		
		intervals = fftFeature.intervalNum;
		database = new int[(int) (fftFeature.sampleSize * fftFeature.intervalNum)];
		count = 0;
		
		for (j = 0; j < intervals; j++) {
			for (i = 0; i < fftFeature.originalSize; i++) {
				if (i < AudioFFTFeature.COEFFICIENT_NUM) {
					y[i] = new Complex(fftFeature.fftSin[j][i], fftFeature.fftCos[j][i]);
				}
				else {
					y[i] = new Complex(0, 0);
				}
			}
			Complex x[] = FFT.ifft(y);
			
			for (i = 0; i < (int)fftFeature.sampleSize; i++) {
				database[count] = (int) x[i].re();
				count++;
			}
		}
		System.out.println("complete rebuild");
	}
	
	public static void rebuildQuery(AudioFFTFeature fftFeature) {
		Complex y[] = new Complex[fftFeature.originalSize];
		
		int i, j, intervals, count;
		
		intervals = fftFeature.intervalNum;
		query = new int[(int) (fftFeature.sampleSize * fftFeature.intervalNum)];
		count = 0;
		
		for (j = 0; j < intervals; j++) {
			for (i = 0; i < fftFeature.originalSize; i++) {
				if (i < AudioFFTFeature.COEFFICIENT_NUM) {
					y[i] = new Complex(fftFeature.fftSin[j][i], fftFeature.fftCos[j][i]);
				}
				else {
					y[i] = new Complex(0, 0);
				}
			}
			Complex x[] = FFT.ifft(y);
			
			for (i = 0; i < (int)fftFeature.sampleSize; i++) {
				query[count] = (int) x[i].re();
				count++;
			}
		}
		System.out.println("complete rebuild");
	}
	
	public static double compareAudio(int databaseStart, int databaseLen) {
		double diff = 0;
		double sub2;
		
		sub2 = (double)databaseStart / (double)databaseLen * database.length;
		
		int index = (int)sub2;
		for (int i = 0; i < query.length; i++) {
			diff+=Math.abs(query[i] - database[index + i]);
		}
		double max_diff = Math.pow(2, 15) * query.length;
		
		return diff/max_diff;
	}*/
}
