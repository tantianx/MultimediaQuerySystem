package tools;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class DataTypeHelper {
	public static int byteToInt(byte b) {
		int result = 0xff & b;
		return result;
	}
	
	public static int twoBytesToInt(byte high, byte low) {
		int result = 0;
		
		result = (high << 8) + (low & 0x00ff);
		
		return result;
	}
	
	public static Mat convertToCVMat(int data[][][], int width, int height) {
		Mat image = new Mat(width, height, CvType.CV_32SC3);
		
		int i, j, k;
		
		for (i = 0; i < width; i++) {
			for (j = 0; j < height; j++) {
				for (k = 0; k < 3; k++) {
					image.put(i, j, data[i][j]);
				}
			}
		}

		return image;
	}
}
