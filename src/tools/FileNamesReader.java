package tools;

import java.io.File;
import java.util.Arrays;

public class FileNamesReader {
	public static String getAudioFileName(String dir) {
		File folder = new File(dir);
		File[] fileListFiles = folder.listFiles();
		
		int j;
		for (j = 0; j < fileListFiles.length; j++) {
			if (fileListFiles[j].isFile() && fileListFiles[j].toString().contains("wav")) {
				break;
			}
		}
		return fileListFiles[j].toString();
	}
	
	public static String[] getImageFileNames(String dir) {
		File folder = new File(dir);
		File[] fileListFiles = folder.listFiles();
		
		int j, count;
		count = 0;
		for (j = 0; j < fileListFiles.length; j++) {
			if (fileListFiles[j].isFile() && fileListFiles[j].toString().contains("rgb")) {
				count++;
			}
		}
		
		String fileNames[] = new String[count];
		count = 0;
		for (j = 0; j < fileListFiles.length; j++) {
			if (fileListFiles[j].isFile() && fileListFiles[j].toString().contains("rgb")) {
				fileNames[count] = fileListFiles[j].toString();
				count++;
			}
		}
		Arrays.sort(fileNames);
		
		return fileNames;
	}
}
