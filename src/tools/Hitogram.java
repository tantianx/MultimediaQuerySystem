package tools;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import com.sun.org.apache.xml.internal.utils.SuballocatedByteVector;

public class Hitogram {
	public static ImageIcon createHitogram(int width, int height, int data[], int max) {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		
		int i, j;
		
		int len = data.length;
		int graphHeight;
		int interval = width / len + 1;
		
		for (i = 0; i < width; i+=interval) {
			
			int sub = (i) / interval;
			System.out.println("i: " + i + " sub: " + sub);
			graphHeight = (int)((float)data[sub] / (float)max * (float)height);
			//System.out.println("h: " + graphHeight);
			for (j = height - 1; j > height - 1 - graphHeight; j--) {
				bufferedImage.setRGB(i, j, 0xff0000);
			}
			for (j = height - 1 - graphHeight; j >= 0; j--) {
				bufferedImage.setRGB(i, j, 0x000000);
			}
		}
		
		return new ImageIcon(bufferedImage);
	}
	
	public static ImageIcon createHSVHitogram(int width, int height, double data1[], double data2[]) {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		
		int len = data1.length;
		int i, j, k;
		int graphHeight;
		double interval = (double)width / (double)len;
		
		int max = -1;
		for (i = 0; i < len; i++) {
			max = (int) (max < data1[i] ? data1[i] : max);
			max = (int) (max < data2[i] ? data2[i] : max);
		}
		
		for (i = 0; i < width; i++) {
			for (j = 0; j < height; j++) {
				bufferedImage.setRGB(i, j, 0x000000);
			}
		}
		
		boolean drawn[] = new boolean[width];
		for (i = 0; i < width; i++) drawn[i] = false;
		
		double scaleFactor = (double)height / (double)max;
		for (i = 0; i < len; i++) {
			graphHeight = (int) (data1[i] * scaleFactor);
			for (j = 0; j < interval / 2; j++) {
				for (k = 0; k < graphHeight - 1; k++) {
					bufferedImage.setRGB((int) (i * interval + j), height - k - 1, 0xff0000);
				}
			}
			
			graphHeight = (int) (data2[i] * scaleFactor);
			for (j = (int) (interval / 2); j < interval; j++) {
				for (k = 0; k < graphHeight - 1; k++) {
					bufferedImage.setRGB((int) (i * interval + j), height - k - 1, 0x0000ff);
				}
			}
		}
		
		return new ImageIcon(bufferedImage);
	}
	
	public static ImageIcon createWaveGraph(int width, int height, double data[]) {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		
		int len = data.length;
		double graphHeight[] = new double[width];
		
		int i, j, max, min;
		
		
		float interval = len / width + 1;
		for (i = 0; i < len; i+=interval) {
			int sub = (int)((float)i / interval);
			graphHeight[sub] = data[i];
		}
		
		int mid = height / 2;
		
		for (i = 0; i < width; i++) {
			if (graphHeight[i] > 0) {
				graphHeight[i] = (int) (graphHeight[i] * height/ 2);
			}
			else {
				graphHeight[i] = (int) (graphHeight[i] * height/ 2);
			}
		}
		
		for (i = 0; i < width; i++) {
			for (j = 0; j < height; j++) {
				bufferedImage.setRGB(i, j, 0x000000);
			}
		}
		
		//for (i = 0; i < data.length; i++) System.out.println(data[i]);
		
		for (i = 0; i < width; i++) {
			//bufferedImage.setRGB(i, mid, 0xff0000);
			if (graphHeight[i] > 0) {
				for (j = 0; j < graphHeight[i] - 1; j++) {
					bufferedImage.setRGB(i, height - j - 1, 0xff0000);
				}
			}
		}
		
		return new ImageIcon(bufferedImage);
	}
}
