package tools;

import multiMediaQuerySystem.MultiMediaQueryApplication;

public class MatchRecord {
	
	public static double diffs[][];
	public static int smallCount[][];
	
	public static double audioDiff[][];
	public static double colorDiff[][];
	public static double motionDiff[][];
	
	public static void init() {
		diffs = new double[7][450];
		smallCount = new int[7][450];
		audioDiff = new double[7][450];
		colorDiff = new double[7][450];
		motionDiff = new double[7][450];
		
		int i, j;
		for (i = 0; i < 7; i++) {
			for (j = 0; j < 450; j++) {
				smallCount[i][j] = 0;
				diffs[i][j] = Double.MAX_VALUE;
			}
		}
	}
	
	// result[0] for catagory, result[1] for position.
	public static void getDiffAt(int rank, int result[]) {
		int i, j;
		for (i = 0; i < 7; i++) {
			for (j = 0; j < 450; j++) {
				if (smallCount[i][j] == rank - 1) {
					result[0] = i;
					result[1] = j;
				}
			}
		}
	}
	
	public static void calculateLargeCount() {
		int i, j, k, l;
		
		double temp;
		int count;
		
		for (i = 0; i < 7; i++) {
			for (j = 0; j < 450; j++) {
				temp = diffs[i][j];
				count = 0;
				
				for (k = 0; k < 7; k++) {
					for (l = 0; l < 450; l++) {
						if (temp > diffs[k][l]) {
							count++;
						}
					}
				}
				
				smallCount[i][j] = count;
			}
		}
	}
	
	public static Object[][] generateTableContent(String ranks[], String names[], String differents[], String af[],
			String cf[], String mf[]) {
		Object data[][] = new Object[100][3];
		
		int count, rank;
		count = 1;
		rank = 0;
		
		ranks[0] = "rank";
		names[0] = "clip name";
		differents[0] = "differences";
		af[0] = "audio diff";
		cf[0] = "main color diff";
		mf[0] = "motion diff";
				
		
		int i, j;
		do {
			rank++;
			for (i = 0; i < 7; i++) {
				for (j = 0; j < 450; j++) {
					if (smallCount[i][j] == rank - 1) {
						ranks[count] = Integer.toString(rank);
						names[count] = MultiMediaQueryApplication.databaseClips[i].clipName + " " + j;
						differents[count] = String.format("%.6f", diffs[i][j]);
						af[count] = String.format("%.6f", audioDiff[i][j]);
						cf[count] = String.format("%.6f", colorDiff[i][j]);
						mf[count] = String.format("%.6f", motionDiff[i][j]);
						count++;
						if (count >= 50) return data;
					}
				}
			}
			
		} while(count <= 50);
		
		return data;
	}
	
	public static void normalize() {
		double amax, amin, cmax, cmin, mmax, mmin;
		int i, j;
		
		amax = cmax = mmax = Double.MIN_VALUE;
		amin = cmin = mmin = Double.MAX_VALUE;
		
		for (i = 0; i < 7; i++) {
			for (j = 0; j < 450; j++) {
				amax = amax > audioDiff[i][j] ? amax : audioDiff[i][j];
				amin = amin < audioDiff[i][j] ? amin : audioDiff[i][j];
				
				cmax = cmax > colorDiff[i][j] ? cmax : colorDiff[i][j];
				cmin = cmin < colorDiff[i][j] ? cmin : colorDiff[i][j];
				
				mmax = mmax > motionDiff[i][j] ? mmax : motionDiff[i][j];
				mmin = mmin < motionDiff[i][j] ? mmin : motionDiff[i][j];
			}
		}
		
		double alen = amax - amin;
		double clen = cmax - cmin;
		double mlen = mmax - mmin;
		
		for (i = 0; i < 7; i++) {
			for (j = 0; j < 450; j++) {
				audioDiff[i][j] = (audioDiff[i][j] - amin) / alen;
				colorDiff[i][j] = (colorDiff[i][j] - cmin) / clen;
				motionDiff[i][j] = (motionDiff[i][j] - mmin) / clen;
				diffs[i][j] = audioDiff[i][j] * 1 + colorDiff[i][j] * 8 + motionDiff[i][j] * 4;
			}
		}
		
	}
}
