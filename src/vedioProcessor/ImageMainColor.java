package vedioProcessor;

import java.awt.Color;

public class ImageMainColor extends VedioFeature {
	
	static public int H_LEVEL_NUM = 16;
	static public int S_LEVEL_NUM = 4;
	static public int V_LEVEL_NUM = 4;
	
	public static int SUBSAMPLE_SIZE = 1;
	
	public double HSV_Count[][][];
	
	@Override
	public double featureDiff(VedioFeature feature2) {
		ImageMainColor mainColor2 = (ImageMainColor) feature2;
		
		double diff1 = 0;
		
		double total = H_LEVEL_NUM * S_LEVEL_NUM * V_LEVEL_NUM;
		double factor = (double)1 / total;
		
		for (int i = 0; i < H_LEVEL_NUM; i++) {
			for (int j = 0; j < S_LEVEL_NUM; j++) {
				for (int k = 0; k < V_LEVEL_NUM; k++) {
					diff1 += Math.abs(this.HSV_Count[i][j][k] - mainColor2.HSV_Count[i][j][k]);
				}
			}
		}
		
		double diff2 = 0;
		
		// Select the max H value from the array.
		int maxSub1, maxSub2, maxH1, maxH2;
		maxSub1 = maxSub2 = 0;
		maxH1 = this.marginalH(0);
		maxH2 = mainColor2.marginalH(0);
		
		for (int i = 1; i < H_LEVEL_NUM; i++) {
			int tempH1 = marginalH(i);
			int tempH2 = mainColor2.marginalH(i);
			if (tempH1 > maxH1) maxSub1 = i;
			if (tempH2 > maxH2) maxSub2 = i;
		}
		
		int index1, index2;
		for (int i = 0; i < H_LEVEL_NUM; i++) {
			index1 = maxSub1 + 1 >= H_LEVEL_NUM ? 0 : maxSub1 + 1;
			index2 = maxSub2 + 1 >= H_LEVEL_NUM ? 0 : maxSub2 + 1;
			
			int tempH1 = marginalH(index1);
			int tempH2 = marginalH(index2);
			
			diff2 += Math.abs(tempH1 - tempH2);
		}
		
		return (diff1 * 0.8 + diff2 * 0.2);
	}
	
	private int marginalH(int h) {
		int result = 0;
		
		int i, j;
		for (i = 0; i < S_LEVEL_NUM; i++) {
			for (j = 0; j < V_LEVEL_NUM; j++) {
				result += this.HSV_Count[h][i][j];
			}
		}
		
		return result;
	}

	@Override
	public void getFeatureFromRawData(int[][][] rawData, int width, int height) {
		// TODO Auto-generated method stub
		float hsv[];
		int r, g, b, subH, subS, subV;
		
		HSV_Count = new double[H_LEVEL_NUM][S_LEVEL_NUM][V_LEVEL_NUM];
		for (int i = 0; i < H_LEVEL_NUM; i++)
			for (int j = 0; j < S_LEVEL_NUM; j++)
				for (int k = 0; k < V_LEVEL_NUM; k++)
					HSV_Count[i][j][k] = 0;
		
		for (int i = 0; i < width; i+=SUBSAMPLE_SIZE) {
			for (int j = 0; j < height; j+=SUBSAMPLE_SIZE) {
				r = rawData[i][j][0];
				g = rawData[i][j][1];
				b = rawData[i][j][2];
				
				hsv = Color.RGBtoHSB(r, g, b, null);
				//System.out.println("h: " + hsv[0] + " s: " + hsv[1] + " v: " + hsv[2]);
				subH = getQuantizedHValue(hsv[0]);
				subS = getQuantizedSValue(hsv[1]);
				subV = getQuantizedVValue(hsv[2]);
				HSV_Count[subH][subS][subV] += getSignificance(i, j, width, height);
			}
		}
		
	}
	
	private double getSignificance(int x, int y, int w, int h) {
		double sig = 0;
		double distanceX = Math.abs(x - w / 2);
		double distanceY = Math.abs(y - h / 2);
		
		sig = (1 - (distanceX + distanceY) / (double)(w / 2 + h / 2)) * 16;
		
		return sig;
	}
	
	private int getQuantizedHValue(float h)
	{
		int sub = 0;
		
		h = h * 360;
		
		if (h <= 15) sub = 0;
		else if (h <= 25) sub = 1;
		else if (h <= 45) sub = 2;
		else if (h <= 55) sub = 3;
		else if (h <= 80) sub = 4;
		else if (h <= 108) sub = 5;
		else if (h <= 140) sub = 6;
		else if (h <= 165) sub = 7;
		else if (h <= 190) sub = 8;
		else if (h <= 220) sub = 9;
		else if (h <= 255) sub = 10;
		else if (h <= 275) sub = 11;
		else if (h <= 290) sub = 12;
		else if (h <= 316) sub = 13;
		else if (h <= 330) sub = 14;
		else if (h <= 345) sub = 15;
		else sub = 0;
		
		return sub;
	}
	
	private int getQuantizedSValue(float s)
	{
		int sub = 0;
		
		if (s <= 0.15) sub = 0;
		else if (s <= 0.4) sub = 1;
		else if (s <= 0.75) sub = 2;
		else sub = 3;
		
		return sub;
	}
	
	private int getQuantizedVValue(float v)
	{
		int sub = 0;
		
		if (v <= 0.15) sub = 0;
		else if (v <= 0.4) sub = 1;
		else if (v <= 0.75) sub = 2;
		else sub = 3;
		
		return sub;
	}
}
