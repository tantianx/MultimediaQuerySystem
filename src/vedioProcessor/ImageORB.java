package vedioProcessor;

import java.beans.FeatureDescriptor;

import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import java.util.*;
import tools.DataTypeHelper;
import org.opencv.features2d.Features2d;

public class ImageORB extends VedioFeature {

	public Mat descriptor; 
	public final int MATCH_SELECTED = 5;
	public final double UNMATCH_PENALTY = 10000000;
	
	@Override
	public double featureDiff(VedioFeature feature2) {
		// TODO Auto-generated method stub
		ImageORB orb2 = (ImageORB) feature2;
		
		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
		MatOfDMatch matches = new MatOfDMatch();
		matcher.match(this.descriptor, orb2.descriptor, matches);
		
		List<DMatch> matchList = matches.toList();
		Iterator<DMatch> itr = matchList.iterator();
		
		int count = 0;
		double distance = 0;
		while (itr.hasNext()) {
			DMatch element = itr.next();
			distance += element.distance;
			count++;
		}
		
		if (count != 0) {
			distance /= count;
		}
		else {
			distance = UNMATCH_PENALTY;
		}
		
		return distance;
	}

	@Override
	public void getFeatureFromRawData(int[][][] rawData, int width, int height) {
		// TODO Auto-generated method stub
		FeatureDetector orb = FeatureDetector.create(FeatureDetector.ORB);		
		Mat image = DataTypeHelper.convertToCVMat(rawData, width, height);
		MatOfKeyPoint keypoints = new MatOfKeyPoint();
		
		image.convertTo(image, CvType.CV_32F);
		orb.detect(image, keypoints);
		DescriptorExtractor descriptorExtractor = DescriptorExtractor.create(org.opencv.features2d.DescriptorExtractor.ORB);
		descriptor = new Mat(width, height, CvType.CV_32SC3);
		descriptor.convertTo(descriptor, CvType.CV_32F);
		descriptorExtractor.compute(image, keypoints, descriptor);
		
		System.out.println("get orb keypoints: " + keypoints.size());
	}
}
