package vedioProcessor;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Imgproc.*;

import sun.net.www.content.audio.x_aiff;
import tools.DataTypeHelper;

public class MotionVector extends VedioFeature {
	
	public static boolean isNewVedio = true; 
	private static int preRawData[][][];
	
	public static final int BLOCK_SIZE = 8;
	public static int STEP_SIZE = 1;
	public static final int RESIDUAL_COEFFICIENT_NUM = 32;
	public static final int IFRAME_THRESHOLD = 10000000;
	public static final int VALID_MOTION_THRESHOLD = 25 * 3 * BLOCK_SIZE * BLOCK_SIZE;
	
	public boolean isIFrame;
	public short motionVectorX[];
	public short motionVectorY[];
	public double residuals[];
	public int motions[];
	public byte iframe[][][];
	
	private static int width;
	private static int height;

	@Override
	public double featureDiff(VedioFeature feature2) {
		MotionVector mv2 = (MotionVector) feature2;
		double diff = 0;
		int motion1, motion2;
		
		for (int i = 0; i < 8; i++) {
			motion1 = this.motions[i];
			motion2 = mv2.motions[i];
			diff += Math.abs(motion1 - motion2);
		}
		
		return diff;
	}
	
	public void initForNewVedio() {
		isNewVedio = true;
	}
	
	public void recordMotion() {
		int i;
		int len = width / BLOCK_SIZE * height / BLOCK_SIZE;
		
		int x, y;
		this.motions = new int[9];
		
		for (i = 0; i < len; i++) {
			x = motionVectorX[i];
			y = motionVectorY[i];
			
			if (residuals[i] > VALID_MOTION_THRESHOLD) continue;
			
			if (y == 0) {
				if (x == 0) motions[0]++;
				else if (x > 0) motions[1]++;
				else motions[5]++;
			}
			else if (x == 0) {
				if (y > 0) motions[7]++;
				else motions[3]++;
			}
			else {
				double k = Math.abs(y) / Math.abs(x);
				double angel = Math.atan(k);
				if (x > 0 && y > 0) {
					if (angel < Math.PI / 8)
						motions[1]++;
					else if (angel < Math.PI / 8 * 3) {
						motions[8]++;
					}
					else motions[7]++;
				}
				else if (x > 0 && y < 0) {
					if (angel < Math.PI / 8)
						motions[1]++;
					else if (angel < Math.PI / 8 * 3) {
						motions[2]++;
					}
					else motions[3]++;
				}
				else if (x < 0 && y < 0) {
					if (angel < Math.PI / 8)
						motions[5]++;
					else if (angel < Math.PI / 8 * 3) {
						motions[4]++;
					}
					else motions[3]++;
				}
				else if (x < 0 && y > 0) {
					if (angel < Math.PI / 8)
						motions[5]++;
					else if (angel < Math.PI / 8 * 3) {
						motions[6]++;
					}
					else motions[7]++;
				}
			}
		}
	}

	@Override
	public void getFeatureFromRawData(int[][][] rawData, int width, int height) {
		int newRawData[][][] = new int[width/2][height/2][3];
		for (int i = 0; i < width / 2; i++) {
			for (int j = 0; j < height / 2; j++) {
				for (int k = 0; k < 3; k++) {
					newRawData[i][j][k] = rawData[i][j][k];
				}
			}
		}
		
		width = width / 2;
		height = height / 2;
		
		rawData = newRawData;
		
		// TODO Auto-generated method stub
		if (preRawData == null)
		 	preRawData = new int[width][height][3];
		
		this.width = width;
		this.height = height;
		
		// Calculate the motion vector.
		if (!isNewVedio) {
			int blockNumX, blockNumY;
			
			blockNumX = width / BLOCK_SIZE;
			blockNumY = height / BLOCK_SIZE;
			motionVectorX = new short[blockNumX * blockNumY];
			motionVectorY = new short[blockNumX * blockNumY];
			residuals = new double[blockNumX * blockNumY];
			
			int i, j;
			
			for (i = 0; i < width; i+= BLOCK_SIZE) {
				for (j = 0; j < height; j+=BLOCK_SIZE) {
					getMotionVectorAndResidual(rawData, width, height, i, j);
				}
			}
		}
		else {
			this.isIFrame = true;
		}
		
		isNewVedio = false;
		
		if (isIFrame) {
			this.motions = new int[9];
			for (int i = 0; i < 9; i++) this.motions[i] = 0;
		}
		else {
			recordMotion();
		}

		preRawData = rawData;
	}
	
	private int getMotionVectorAndResidual(int[][][] data, int width, int height, int startX, int startY) {
		int blockDiffs, minBlockDiffs;
		int i, j, matchX, matchY;
		
		minBlockDiffs = 100000000;
		matchX = startX;
		matchY = startY;
		
		// Find the block in previous frame with least error.
		int dx[] = {0, 1, 1,  1,  0, -1, -1, -1};
		int dy[] = {1, 1, 0, -1, -1, -1,  0,  1};
		
		boolean checkedBlock = false;
		
		int testX, testY;
		int loopCount = 0;
		
		testX = startX;
		testY = startY;
		
		// System.out.println("start x, y:" + startX + " " + startY);
		do {
			checkedBlock = false;
			for (i = 0; i < 8; i++) {
				testX = startX + loopCount * dx[i];
				testY = startY + loopCount * dy[i];
				// System.out.println("test x, y:" + testX + " " + testY);
				
				if (validBlock(testX, testY, width, height)) {
					checkedBlock = true;
					blockDiffs = getBlockDiff(data, startX, startY, testX, testY);
					if (blockDiffs < minBlockDiffs) {
						minBlockDiffs = blockDiffs;
						matchX = testX;
						matchY = testY;
					}
				}
			}
			loopCount+=STEP_SIZE;
		} while(checkedBlock);
		
		int blockIndex = hashIndex(startX, startY, width, height);
		this.motionVectorX[blockIndex] = (short) (matchX - startX);
		this.motionVectorY[blockIndex] = (short) (matchY - startY);
		this.residuals[blockIndex] = minBlockDiffs;
		
		return minBlockDiffs;
	}
	
	/* 
	 * x1, y1 is the top left pixel of the current image. 
	 * x2, y2 is the top left pixel of the previous image.
	 */
	private int getBlockDiff(int[][][] data, int x1, int y1, int x2, int y2) {
		int diff = 0;
		
		int i, j, k;
		for (i = 0; i < BLOCK_SIZE; i++) {
			for (j = 0; j < BLOCK_SIZE; j++) {
				for (k = 0; k < 3; k++) {
					diff += Math.abs(data[x1 + i][y1 + j][k] - preRawData[x2 + i][y2 + j][k]);
				}
			}
		}
		
		return diff;
	}
	
	private int hashIndex(int x, int y, int width, int height) {
		if (x < 0 || y < 0 || x >= width || y >= height) return -1;
		int blockNumX = width / BLOCK_SIZE;
		
		int a = x / BLOCK_SIZE;
		int b = y / BLOCK_SIZE;
	
		return b * blockNumX + a;
	}
	
	private boolean validBlock(int x, int y, int width, int height) {
		if (x < 0 || y < 0) return false;
		
		if (x + BLOCK_SIZE - 1 >= width - 1) return false;
		if (y + BLOCK_SIZE - 1 >= height - 1) return false;
		
		return true;
	}
}
