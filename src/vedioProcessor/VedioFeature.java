package vedioProcessor;

public abstract class VedioFeature {
	double weight;
	String featureName;
	
	public abstract double featureDiff(VedioFeature feature2);
	public abstract void getFeatureFromRawData(int rawData[][][], int width, int height);
}
