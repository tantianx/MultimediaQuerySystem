package vedioProcessor;

public class VedioInfo {
	private VedioFeature features[];
	private String fileName;
	private int width;
	private int height;
	
	public VedioInfo(int width, int height, String filename)
	{
		this.width = width;
		this.height = height;
		this.fileName = filename;
	}
	
	public void extractImageFeaturesFromRawData(int rawData[][][])
	{
		
	}
	
	public double compareToAnotherImage(VedioInfo info2)
	{
		double result = 0;
		
		for (int i = 0; i < features.length; i++)
		{
			result += this.features[i].weight * this.features[i].featureDiff(info2.features[i]);
		}
		
		return result;
	}
}
