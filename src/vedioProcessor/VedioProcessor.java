package vedioProcessor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import tools.DataTypeHelper;
import multiMediaQuerySystem.ClipInfo;

public class VedioProcessor {
	Vector<VedioInfo> imageInfos;
	public static int width = 352;
	public static int height = 288;
	
	public VedioProcessor()
	{
		imageInfos = new Vector<VedioInfo>();
	}
	
	public void getImageFeaturesFromFiles(String filenames[], ClipInfo clip) throws IOException
	{
		int len = filenames.length;
		String temps[] = filenames;
		clip.initImageFeatures(filenames.length);
		
		int rawData[][][];
		
		clip.motionVector[0].initForNewVedio();
		for (int i = 0; i < len; i++) {
			System.out.println("Processing: " + filenames[i]);
			rawData = getImageRawData(filenames[i], null);
			clip.mainColor[i].getFeatureFromRawData(rawData, width, height);
			clip.motionVector[i].getFeatureFromRawData(rawData, width, height);
			//clip.orbs[i].getFeatureFromRawData(rawData, width, height);
			//System.out.println("Free memory (bytes): " +  Runtime.getRuntime().freeMemory());
		}
		clip.imageFileNames = temps;
	}
	
	public BufferedImage getImageBufferedImageFromFile(String filename)
	{
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		try {
			getImageRawData(filename, img);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return img;
	}
	
	public void readImageFromFile(String filename) throws IOException
	{
		int rawData[][][] = getImageRawData(filename, null);
		
		VedioInfo imageInfo = new VedioInfo(width, height, filename);
		imageInfo.extractImageFeaturesFromRawData(rawData);
		imageInfos.add(imageInfo);
	}
	
	private int[][][] getImageRawData(String filename, BufferedImage img) throws IOException
	{
		int rawData[][][] = new int [width][height][3];
		
		try {
			File file = new File(filename);
			InputStream is = new FileInputStream(file);
			
			long len = file.length();
		    byte[] bytes = new byte[(int)len];
		    
		    int offset = 0;
	        int numRead = 0;
	        while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	            offset += numRead;
	        }
	        
	    	byte r, g, b;
	    	
	    	int ind = 0;
	    	
			for(int y = 0; y < height; y++){
				for(int x = 0; x < width; x++){
					r = bytes[ind];
					g = bytes[ind+height*width];
					b = bytes[ind+height*width*2];
					
					rawData[x][y][0] = DataTypeHelper.byteToInt(r);
					rawData[x][y][1] = DataTypeHelper.byteToInt(g);
					rawData[x][y][2] = DataTypeHelper.byteToInt(b);
					int pix = 0xff000000 | ((rawData[x][y][0] & 0xff) << 16) | ((rawData[x][y][1] & 0xff) << 8) | (rawData[x][y][2] & 0xff);
					
					if (img != null)
					{
						img.setRGB(x, y, pix);
					}
					
					ind++;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rawData;
	}
}
